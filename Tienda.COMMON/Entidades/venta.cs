﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.COMMON.Entidades
{
    public class venta : BaseDTO
    {
        public int IdVenta { get; set; }
        public DateTime FechaUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public object FechaHora { get; internal set; }
        public string Cliente {get; set;}
    }
}
