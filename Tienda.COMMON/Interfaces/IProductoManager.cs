﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona los metodos relacionados a los productos
    /// </summary>
  public interface IProductoManager:IGenericRepository<producto>
    {
        IEnumerable<producto> BuscarProductosPorNombre(string criterio);


        producto BuscarProductoPorNombreExacto(string nombre);
    } 
}
