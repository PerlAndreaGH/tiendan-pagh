﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona los metodo relacionado a los usuarios
    /// </summary>
    public  interface IUsuarioManager: IGenericRepository<usuario>
    {
        /// <summary>
        /// Verifica si las credenciales son validas para el usuario
        /// </summary>
        /// <param name="nombreUsuario">Nombre de usuario</param>
        /// <param name="password">Contraseña de usuario</param>
        /// <returns>Si las credenciales son correctas regresa el usuario completo, de otro modo regresa null</returns>
        usuario Login(string nombreUsuario, string password);

    }
}
