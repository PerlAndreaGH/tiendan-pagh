﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona los metodos relacionado a los productos vendidos en las ventas
    /// </summary>
    public interface IProductoVendidoManager: IGenericManager<productovendido>

    {
        /// <summary>
        /// Obtiene los productos contenidos en una venta
        /// </summary>
        /// <param name="IdVenta">Id de la venta </param>
        /// <returns>Conjunto de productos contenidos en la venta</returns>
        IEnumerable<productovendido> ProductosDeUnaVenta(int IdVenta);


    }
}
