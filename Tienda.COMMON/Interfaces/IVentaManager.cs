﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    /// <summary>
    /// metodos relacionados a las ventas 
    /// </summary>
    public interface IVentaManager:IGenericRepository<venta>
    {
        /// <summary>
        /// obtiene todas las ventas del intervalo especificado 
        /// </summary>
        /// <param name="inicio">Fecha de inicio</param>
        /// <param name="fin">Fecha de fin</param>
        /// <returns>Conjunto de ventas efectuadas en el intervalo proporcionado</returns>
        IEnumerable<venta> VentasEnIntervalo(DateTime inicio, DateTime fin);
        /// <summary>
        /// Obtiene las ventas a un cliente en un intervalo especificado
        /// </summary>
        /// <param name="nombreCliente">Nombre del cliente</param>
        /// <param name="inicio">Fecha de inicio</param>
        /// <param name="fin">Fecha de fin</param>
        /// <returns>Conjunto de ventas realizadas al cliente en un intervalo especifico</returns>
        IEnumerable<venta> VentasDeClienteEnIntervalos(string nombreCliente, DateTime inicio, DateTime fin);

    }
}
