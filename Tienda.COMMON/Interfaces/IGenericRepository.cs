﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona metodos basicos de acceso a una tabla de base de datos
    ///</summary>
    ///<typeparam name="T">tipo de entidad (clase) a la que se refiere una tabla</typeparam>
    public interface IGenericRepository<T> where T:BaseDTO
    {
        /// <summary>
        /// Proporciona informacion sobre el error ocurrido en alguna de las operaciones
        /// </summary>
        string Error { get; }
        /* 
         * CRUD -> Create (insert), Read (select), Update (update), Delete (delete)
         */
        /// <summary>
        /// Inserta una entidad en la tabla 
        /// </summary>
        /// <param name="entidad"> Entidad a insertar </param>
        /// <returns>Confirmacion de la Inserccion </returns>
        bool Create (T entidad);
        /// <summary>
        /// Obtiene todos los registros en la tabla 
        /// </summary>
        IEnumerable<T> Read { get; }
        /// <summary>
        /// Actualiza un registro en la tabla en base a la propiedad Id
        /// </summary>
        /// <param name="entidad"> Entidad ya modificada, el Id debe existir en la tabla para modificarse  </param>
        /// <returns>Confirmacion en la actualizacion </returns>
        bool Update(T entidad);
        /// <summary>
        /// Elimina una entidad en la base de datos de acuerdo al Id relacionado
        /// </summary>
        /// <param name="entidad"> id de la entidad a eliminar </param>
        /// <returns>Confirmacion de la Eliminacion </returns>
        bool Delete(string id);
        // Query -> Reaizar consultas de acuerdo a la tabla, mediante expresiones lambda
        /// <summary>
        /// Realiza una consulta personalizada a la tabla 
        /// </summary>
        /// <param name="predicado">Expression Lambda que dfine la consulta</param>
        /// <returns>Conjunto de entidades que cumplen con la consulta</returns>
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);
        /// <summary>
        /// Obtener una entidad en base a su Id
        /// </summary>
        /// <param name="id"> Id de la entidad a obtener </param>
        /// <returns>Entidad completa que le corresponde el id proporcionado</returns>
        T SearcById (string id);
    }
}
