﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona metodo estandarizados para el acceso a tablas; cada manager creado debe implementar de esta interfaz
    /// <typeparamref name="T"/>tipo de entidad de la cual se implementa el Manager </typeparam>
    /// </summary>
    public  interface IGenericManager<T> where T:BaseDTO
    {
        /// <summary>
        /// Proporciona el error relacionado despues de alguna operacion
        /// </summary>
        string Error { get; }
        /// <summary>
        /// Inserta una entidad en la tabla
        /// </summary>
        /// <param name="entidad">entidad a insertar</param>
        /// <returns>Confirmacion de la inserccion</returns>
        bool Insertar(T entidad);
        /// <summary>
        /// Obtiene todos los registros de la tabla
        /// </summary>
        IEnumerable<T> ObtenerTodos { get; }
        /// <summary>
        /// Actualzia un registro en la tabla en base a su propiedad Id
        /// </summary>
        /// <param name="entidad">Entidad ya modificada, el id debe existir en la tabla </param>
        /// <returns></returns>
        bool Actualizar(T entidad);
        /// <summary>
        /// Elimina una entidad en base al Id proporcionado
        /// </summary>
        /// <param name="entidad">Id de la entidad a eliminar</param>
        /// <returns>Confirmacion de la eliminacion</returns>
        bool Eliminar(T entidad);
        /// <summary>
        /// Obtiene un elemento de acuerdo a su Id
        /// </summary>
        /// <param name="id"> Id del elemento a obtener </param>
        /// <returns>Entidad completa correspondiente al Id proporcionado</returns>
        T BuscarPorId(string id);

    }
}
