﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Interfaces;
using System.Data.SqlClient;

namespace Tienda.DAL.MSSqlServer
{
    public class DBMSSqlServer : IDB
    {
        SqlConnection connection;
        public string Error { get; private set; }

        public DBMSSqlServer()
        {
            string server = @"LAPTOP-LE4BS6FM\SQLEXPRESS";
            string db = "tiendan";
            string usr = "tiendausr";
            string pass = "12345";
            connection = new SqlConnection($"Data Source={server}; Initial Catalog={db};Persist Security Info=True; User Id={usr}; Password{pass}");

            Conectar();
        }
        private bool Conectar()
        {
            try
            {
                connection.Open();
                Error = "";
                return true;
            }
            catch(SqlException ex)
            {
                Error = ex.Message;
                return false;
            }

        }
        public bool Comando(string command)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command, connection);
                cmd.ExecuteNonQuery();
                Error = "";
                return true;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
                throw;
            }
        }

        public object Consulta(string consulta)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(consulta, connection);
                SqlDataReader dr = cmd.ExecuteReader();
                Error = "";
                return dr;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }

    public class IDB
    {
        public string Error { get; internal set; }

        internal bool Comando(string sql)
        {
            throw new NotImplementedException();
        }

        internal SqlDataReader Consulta(string sql)
        {
            throw new NotImplementedException();
        }
    }
}
