﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Interfaces;

namespace Tienda.DAL.MySQL
{
    public class DBMySQL : IDB
    {
        private MySqlConnection conexion;
        private object ex;

        public DBMySQL()
        {
            string server = "localhost";
            string datebase = "tienda";
            string uid = "root";
            string password = "";
            conexion = new MySqlConnection(string.Format("SERVER={0};DATABASE={1};UID={2};PASSWORD={3}; SslMode=none;", server, datebase, uid, password));

            Conectar();

        }

        private bool Conectar()
        {
            try {
                conexion.Open();
                Error = "";
                return true;
            } catch (MySqlException ex)
            {
                Error = ex.Message;
                return false;
            }
           // throw new NotImplementedException();
        }

        public string Error { get; private set;}

        public bool Comando(string command)
        {
            try
            {
                MySqlCommand cmd = new MySqlCommand(command, conexion);
                cmd.ExecuteNonQuery();
                Error = "";
                return true;

            }catch (Exception ex)
            {
                Error = ex.Message;
                return false;
                throw;
            }
            //throw new NotImplementedException();
        }

        public object Consulta(string consulta)
        {
            try {
                MySqlCommand cmd = new MySqlCommand(consulta, conexion);
                MySqlDataReader dr = cmd.ExecuteReader();
                Error = "";
                return dr;
            
            } catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
           // throw new NotImplementedException();
        }
        ~DBMySQL()
        {
            if(conexion.State == System.Data.ConnectionState.Open)
            {
                conexion.Close();
            }
        }
    }
}
